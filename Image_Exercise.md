# Fetch Rewards Coding Exercise - Image Coordinates

Your objective in this challenge is to write a program that calculates pixel coordinate values for 
an image that is to be displayed on a two dimensional surface given the dimensions of the image 
and the corner points of the image as it is to be displayed. 

For example, if an image is defined by a 3x3 grid of pixel values, and the (x, y) coordinates
of the four corner points to display the image at are: (1, 1), (3, 1), (1, 3), and (3, 3) then the 
program should calculate and return the coordinates: (1, 1), (2, 1), (3, 1), (1, 2),
(2, 2), (3, 2), (3, 1), (3, 2), (3, 3) which are the coordinates at which to place the 9 pixels
in the image such that they're evenly spaced within the corner points. The solution can 
be seen visually in the image below. 

![img_1.png](img_1.png)

## Input Specifications

The program will take two inputs: 

### 1. Image dimensions
This will be a tuple defining the height and width of the image in terms of pixel counts.

For example, an input for this parameter of (10, 12) means that the image has 10 rows
and 12 columns. 
      
### 2. Corner Points 
   
This will a list of two-element tuples defining the x and y coordinates of the image corner points of the displayed image. It 
consists of four (x, y) pairs.

The specification will follow the following example format:
        
```python
    corner_points = [(1.5, 1.5),  # (x, y)
                     (4.0, 1.5),  # (x, y)
                     (1.5, 8.0),  # (x, y)
                     (4.0, 8.0)]  # (x, y)
```
These corner points are represented visually in the plot below. 

![img.png](img.png)


## Output Specifications
Your program should calculate and return the x and y coordinates at which to plot each pixel
in the input image such that the pixels are evenly spaced within the rectangle defined
by the corner points. 

The output should be of shape mxnx2 where m is the number of rows in the input image and n is 
the number of columns in the input image. The solution in the example at the beginning of this
page would be:
```python
    solution = [[[1.0, 3.0], [2.0, 3.0], [3.0, 3.0]],
                [[1.0, 2.0], [2.0, 2.0], [3.0, 2.0]],
                [[1.0, 1.0], [2.0, 1.0], [3.0, 1.0]]]
```
For example, the x coordinate of the pixel at the top left corner of the image would be accessed with
`solution[0, 0, 0]` and the y coordinate of the pixel at the bottom right of the image would be
accessed with `solution[3, 3, 1]`.

## Assumptions & Requirements
- Your program can assume that the corner points will define a rectangle with sides that are parallel
to the x and y axes (the rectangle will not be rotated)
- The corner points can be provided in any order, your program should determine which is the bottom left, 
top right etc.
- You aren't allowed to develop your solution using any image processing libraries, however please
feel free to use array manipulation libraries such as NumPy
- To enable us to run your program please package the application as a web service that
performs that calculations in response to a POST request containing the inputs in the body of the
  payload. You may use external libraries (i.e, Flask).
- Additionally, please package the web service in a Docker container that can either be built
locally or pulled down and run via Docker hub.
- Please submit your exercise by providing a link to a public repository (i.e., GitHub, Bitbucket)
to your recruiter. The repository should include instructions for running your code
  
## FAQs

### How will this exercise be evaluated? 
An engineer will review the code you submit. At a minimum they must be able to run the program, and the program must produce the expected results. You should provide any necessary documentation within the repository. While your solution does not need to be fully production ready, you are being evaluated so put your best foot forward!

### I have questions about the problem statement.
For any requirements not specified above, use your best judgement to determine expected result. You can elaborate on your decisions via the documentation you provide in your repo.

### Can I provide a private repository?
If at all possible, we prefer a public repository because we do not know which engineer will be evaluating your submission. Providing a public repository ensures a speedy review of your submission. If you are still uncomfortable providing a public repository, you can work with your recruiter to provide access to the reviewing engineer.

### How long do I have to complete the exercise?
There is no time limit for the exercise. Out of respect for your time, we designed this exercise with the intent that it should take you a few hours. But, please take as much time as you need to complete the work.