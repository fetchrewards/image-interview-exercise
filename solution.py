import numpy as np
import matplotlib.pyplot as plt

def find_pixel_locs(corner_points, input_image_shape):
    corner_points = np.array(corner_points)
    locations = np.zeros((input_image_shape[0], input_image_shape[1], 2))
    x_coords = np.linspace(min(corner_points[:, 0]), max(corner_points[:, 0]), input_image_shape[1])
    y_coords = np.linspace(min(corner_points[:, 1]), max(corner_points[:, 1]), input_image_shape[0])
    coords = np.meshgrid(x_coords, y_coords)
    locations[:, :, 0] = coords[0]
    locations[:, :, 1] = coords[1]
    locations = np.flip(locations, axis=0)
    return locations

def plot_solution(corner_points, solution_coords):
    plt.figure()
    plt.scatter(solution_coords[:, :, 0], solution_coords[:, :, 1], c='blue', s=30)
    plt.scatter(corner_points[:, 0], corner_points[:, 1], edgecolors='red', facecolors='none', s=140, linewidths=2)
    plt.show()


if __name__ == '__main__':
    corner_points = [[1.0, 1.0],
                     [1.0, 3.0],
                     [3.0, 1.0],
                     [3.0, 3.0]]

    input_image_shape = (3, 3)


    solution = find_pixel_locs(corner_points, input_image_shape)
    plot_solution(np.array(corner_points), solution)
