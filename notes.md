### Some thoughts and notes around the development of this exercise:

This exercise differs from the text similarity exercise in that is a single correct
output for each input to the problem. I expect that most candidates would get 
to the correct answer because it's not super challenging and it's easy to verify 
that the program is working correctly, but I could be wrong and it will be interesting to track. 

Given that it shouldn't be too challenging to arrive at the correct solution, 
there will be lots of room to evaluate the approach the candidate takes to get to
the solution. There are several ways to calculate the solution in a clean and efficient way
using NumPy functions. If a candidate hacks together a solution using lists and for-loops
it would likely be a less optimal solution. 

It took me about 40 minutes to put together a solution. About half that time was reading 
NumPy docs to find way to do what I was looking for. The other half of the time
was testing to make sure all of the indexing was correct and fixing minor errors. 

If we want to go forward with this exercise, I'll put together some test cases to evaluate
the correctness of the solution for varying inputs as well as runtime complexity
as the dimension of the image scales up. 